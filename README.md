To format
```
find src/ -name '*.purs' -exec purty --write {} \;
```

To export dhall packages to a `local` psc-package set so pulp can compile
```
spago psc-package-insdhall
```

To bundle the client
```
pulp browserify --to bundle.js -m Client
```

To run the server
```
spago run
```
