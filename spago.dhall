{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name =
    "net-worthier"
, dependencies =
    [ "console"
    , "debug"
    , "dotenv"
    , "effect"
    , "express"
    , "foreign-generic"
    , "node-fs-aff"
    , "psci-support"
    , "pux"
    , "webdriver"
    ]
, packages =
    ./packages.dhall
, sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
}
