module App.State
  ( Currency(..)
  , MoneyData(..)
  , NameAmount(..)
  , State(..)
  , initState
  ) where

import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import Data.Newtype (class Newtype)
import Data.Show (class Show)
import Foreign.Generic (genericDecode, genericEncode)
import Foreign.Generic.Class (class Encode, class Decode, defaultOptions)

data Currency
  = GBP
  | JPY

data NameAmount
  = NameAmount String String

newtype MoneyData
  = MoneyData (Array NameAmount)

newtype State
  = State
  { count :: Int
  , currency :: Currency
  , moneyData :: MoneyData
  }

derive instance genericCurrency :: Generic Currency _

derive instance genericNameAmount :: Generic NameAmount _

derive instance genericMoneyData :: Generic MoneyData _

derive instance genericState :: Generic State _

derive instance newtypeMoneyData :: Newtype MoneyData _

derive instance newtypeState :: Newtype State _

instance encodeCurrency :: Encode Currency where
  encode = genericEncode defaultOptions

instance decodeCurrency :: Decode Currency where
  decode = genericDecode defaultOptions

instance decodeNameAmount :: Decode NameAmount where
  decode = genericDecode defaultOptions

instance encodeNameAmount :: Encode NameAmount where
  encode = genericEncode defaultOptions

instance genericMoneyDataDecode :: Decode MoneyData where
  decode = genericDecode defaultOptions

instance genericMoneyDataEncode :: Encode MoneyData where
  encode = genericEncode defaultOptions

instance showCurrency :: Show Currency where
  show = genericShow

instance showNameAmount :: Show NameAmount where
  show = genericShow

instance showMoneyData :: Show MoneyData where
  show = genericShow

instance showState :: Show State where
  show = genericShow

initState :: State
initState =
  State
    { count: 0
    , currency: GBP
    , moneyData: MoneyData []
    }
