module App.App
  ( initApp
  , render
  ) where

import Prelude hiding (div)
import App.Event (Event, foldp)
import App.State (initState, MoneyData, State(..))
import App.View.Finances (view)
import Data.Newtype (over)
import Effect (Effect)
import Pux (EffModel, start)
import Pux.DOM.HTML (HTML)
import Pux.Renderer.React (renderToString)
import Signal (Signal)

initApp ::
  { foldp :: Event -> State -> EffModel State Event
  , initialState :: State
  , view :: State -> HTML Event
  , inputs :: Array (Signal Event)
  }
initApp =
  { initialState: initState
  , view
  , foldp
  , inputs: []
  }

render :: MoneyData -> Effect String
render moneyData = do
  app <- start $ initApp { initialState = over State _ { moneyData = moneyData } initState }
  renderToString app.markup
