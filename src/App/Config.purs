module App.Config where

import Prelude
import Data.Maybe (fromJust)
import Effect (Effect)
import Node.Process (lookupEnv)
import Partial.Unsafe (unsafePartial)

type Config
  = { customerNumber :: String
    , memorableData :: String
    , digits :: String
    }

readConfig :: Effect Config
readConfig =
  { customerNumber: _
  , memorableData: _
  , digits: _
  }
    <$> (unsafeFromJust <$> lookupEnv "CUSTOMER_NUMBER")
    <*> (unsafeFromJust <$> lookupEnv "MEMORABLE_DATA")
    <*> (unsafeFromJust <$> lookupEnv "DIGITS")
  where
  unsafeFromJust ma = unsafePartial (fromJust ma)
