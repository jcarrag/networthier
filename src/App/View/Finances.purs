module App.View.Finances
  ( view
  ) where

import Prelude hiding (div)
import App.Event (Event(..))
import App.State (NameAmount(..), State(..))
import Data.Foldable (for_)
import Data.Newtype (unwrap)
import Pux.DOM.Events (onClick)
import Pux.DOM.HTML (HTML)
import Text.Smolder.HTML (button, div, span, ul)
import Text.Smolder.Markup (text, (#!))

view :: State -> HTML Event
view (State { count, moneyData }) =
  div do
    div do
      button #! onClick (const Increment) $ text "Increment"
      span $ text (show count)
      button #! onClick (const Decrement) $ text "Decrement"
    ul do
      for_ (unwrap moneyData) \(NameAmount name amount) ->
        div do
          text name
          text amount
