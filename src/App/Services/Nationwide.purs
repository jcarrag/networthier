module App.Services.Nationwide
  ( login
  ) where

import Prelude
import Control.Monad.Reader (ask)
import Control.Monad.Trans.Class (lift)
import Data.Array ((..), fromFoldable, length)
import Data.Maybe (fromJust, Maybe)
import Data.String.CodeUnits as S
import Data.String.Unsafe (charAt)
import Data.Time.Duration (Milliseconds(..))
import Data.Traversable (traverse)
import Data.TraversableWithIndex (traverseWithIndexDefault)
import Effect.Aff (delay)
import Partial.Unsafe (unsafePartial)
import Selenium.Monad (byClassName, byName, byXPath, clickEl, findChild, findChildren, findElements, findExact, get, getText, Selenium, sendKeysEl)
import Selenium.Types (Element)
import App.Config (Config)
import App.State (NameAmount(..), MoneyData(..))

login :: forall e. Selenium e ( config :: Config ) MoneyData
login = do
  get "http://nationwide.co.uk/login"
  enterCredentials
  loginButtonL <- byXPath $ "//button[contains(., 'Log in')]"
  loginButtonE <- findExact loginButtonL
  clickEl loginButtonE
  getAmounts

getAmounts :: forall e o. Selenium e o MoneyData
getAmounts = do
  accountRowsL <- byXPath $ "//tr[@class='account-row']"
  accountRowsE <- findElements accountRowsL
  amounts <- traverse getNameAndAmount (1 .. (length $ fromFoldable accountRowsE))
  pure $ MoneyData amounts
  where
  getNameAndAmount i = do
    nameL <- byXPath $ "//table[" <> (show i) <> "]/tbody/tr[2]/td/form/table/tbody/tr[1]/td[1]/a[2]/b"
    nameE <- findExact nameL
    name <- getText nameE
    amountL <- byXPath $ "(//td[@class='bal-details']/strong)[" <> (show i) <> "]"
    amountE <- findExact amountL
    amount <- getText amountE
    pure $ NameAmount name amount

enterCredentials :: forall e. Selenium e ( config :: Config ) Unit
enterCredentials = do
  customerNumber <- _.config.customerNumber <$> ask
  customerNumberL <- byName "CustomerNumber"
  customerNumberI <- findExact customerNumberL
  sendKeysEl customerNumber customerNumberI
  lift $ delay $ Milliseconds 1000.0
  selectSecretDigitsL <- byXPath $ "(//label[@class='choice-list__option__description'])[2]"
  selectSecretDigitsE <- findExact selectSecretDigitsL
  clickEl selectSecretDigitsE
  memorableDataInputL <- byName "MemorableData"
  memorableDataInputE <- findExact memorableDataInputL
  memorableData <- _.config.memorableData <$> ask
  sendKeysEl memorableData memorableDataInputE
  lift $ delay $ Milliseconds 2000.0
  secretDigitsInputGroupL <- byClassName "control control--complex passnumber-partial-entry control--enabled control--loaded"
  secretDigitsInputGroupE <- findExact secretDigitsInputGroupL
  secretDigitsInputListL <- byClassName "control control--no-description control--no-help control--simple control--defer-validation selection-list-control control--enabled control--loaded"
  secretDigitsInputListE <- findChildren secretDigitsInputGroupE secretDigitsInputListL
  void $ traverseWithIndexDefault selectDropDown secretDigitsInputListE

selectDropDown :: forall e. Int -> Element -> Selenium e ( config :: Config ) Unit
selectDropDown i el = do
  dropDownGroupL <- byClassName "control__label__title"
  dropDownE <- map unsafeFromJust $ findChild el dropDownGroupL
  secretDigitIndex <- getText dropDownE
  dropDownOptionsL <- byClassName "control__input"
  dropDownOptionsE <- map unsafeFromJust $ findChild el dropDownOptionsL
  clickEl dropDownOptionsE
  secretDigits <- _.config.digits <$> ask
  let
    secretDigit = unsafePartial indexToSecretDigit secretDigitIndex secretDigits
  secretDigitIndexOptionL <- byXPath $ "(//option[@value='" <> secretDigit <> "'])[" <> (show $ i + 1) <> "]"
  secretDigitIndexOptionE <- findExact secretDigitIndexOptionL
  clickEl secretDigitIndexOptionE

unsafeFromJust :: forall a. Maybe a -> a
unsafeFromJust ma = unsafePartial (fromJust ma)

indexToSecretDigit :: Partial => String -> String -> String
indexToSecretDigit index digits =
  S.singleton
    $ (flip charAt) digits case index of
        "1st digit" -> 0
        "2nd digit" -> 1
        "3rd digit" -> 2
        "4th digit" -> 3
        "5th digit" -> 4
        "6th digit" -> 5
