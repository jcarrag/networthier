module App.Event
  ( Event(..)
  , foldp
  ) where

import Prelude
import App.State (State(..))
import Data.Newtype (unwrap)
import Pux (EffModel)

data Event
  = Increment
  | Decrement

foldp :: Event -> State -> EffModel State Event
foldp Increment state = { state: State $ (unwrap state) { count = (unwrap state).count + 1 }, effects: [] }

foldp Decrement state = { state: State $ (unwrap state) { count = (unwrap state).count - 1 }, effects: [] }
