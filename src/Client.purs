module Client where

import Prelude
import App.State (State, initState)
import App.App (initApp)
import Control.Monad.Except (runExcept)
import Data.Either (either)
import Effect (Effect)
import Foreign.Generic (defaultOptions, genericDecode, Foreign)
import Pux (start)
import Pux.Renderer.React (renderToDOM)

foreign import puxInitialState :: Effect Foreign

readState :: Foreign -> State
readState json = either (\_ -> initState) identity $ runExcept (genericDecode (defaultOptions { unwrapSingleConstructors = true }) json)

main :: Effect Unit
main = do
  puxInitialState' <- puxInitialState
  let
    state = readState puxInitialState'
  app <- start $ initApp { initialState = state }
  renderToDOM "#app" app.markup app.input
  pure unit
