module Main where

import Prelude
--import Control.Monad.Reader (runReaderT)
import Data.Newtype (unwrap)
import Data.Time.Duration (Milliseconds(..))
import Dotenv (loadFile)
import Effect (Effect)
import Effect.Aff (launchAff_)
import Effect.Class (liftEffect)
import Effect.Console (log)
import Foreign.Generic (defaultOptions, genericEncodeJSON)
import Node.Express.App (AppM, listenHttp, get)
import Node.Encoding (Encoding(UTF8))
import Node.FS.Aff (readTextFile)
--import Selenium (close)
--import Selenium.Browser (Browser(..))
--import Selenium.Builder (browser, build)
import App.Config (readConfig)
--import App.Services.Nationwide as Nationwide
import App.App as WA
import App.State (initState, MoneyData(..), NameAmount(..), State(..))
import App.View.HTMLWrapper (htmlWrapper)
import Node.Express.Handler (HandlerM)
import Node.Express.Response (send)
import Pux.Renderer.React (renderToStaticMarkup)
import Signal (constant)

appHandler :: MoneyData -> HandlerM Unit
appHandler moneyData = do
  let
    state = State $ (unwrap initState) { moneyData = moneyData }
  let
    opts = defaultOptions { unwrapSingleConstructors = true }
  let
    stateJson = genericEncodeJSON opts state
  let
    wrappedStateJson =
      "window.__puxInitialState = "
        <> stateJson
        <> ";"
  app_html <- liftEffect $ WA.render moneyData
  html <- liftEffect $ renderToStaticMarkup $ constant (htmlWrapper app_html wrappedStateJson)
  send html

app :: String -> MoneyData -> AppM Unit
app bundle moneyData = do
  get "/" $ appHandler moneyData
  get "/bundle.js" $ send bundle

main :: Effect Unit
main =
  launchAff_ do
    --driver <- build $ browser Chrome
    _ <- loadFile
    let
      defaultTimeout = Milliseconds 1000.0
    config <- liftEffect readConfig
    --nationwide <- runReaderT Nationwide.login { config, driver, defaultTimeout }
    --liftEffect $ log $ "Got amounts: " <> (show nationwide)
    --close driver
    bundle <- readTextFile UTF8 "bundle.js"
    let
      nationwide' = MoneyData [ NameAmount "Nationwide" "$100" ]
    _ <-
      liftEffect
        $ listenHttp (app bundle nationwide') 8080 \_ ->
            log $ "Listening on 8080"
    pure unit
